mef\Supervisor - Keep processes running
=======================================

mef\supervisor runs indefinitely, monitoring the specified program to make sure
it is always running. It is responsible for launching the program itself.

To use:

    php bin/supervisor.php --command="path to your command"

Command line options:

* `command`: the fully qualified path to run
* `workers`: the number of instances to launch & monitor (default: 1)
* `success-delay`: the number of seconds to wait before relaunching an instance
that quit gracefully
* `error-delay`: the number of seconds to wait before relaunching an instance
that died unexpectedly
* `silent`: do not display any output from the workers
* `quiet`: do not display any informational output from supervisor

mef\supervisor is not [Supervisor](http://supervisord.org/). If you need
production-quality process management, then use Supervisor!

TODO:

* move the app logic into classes