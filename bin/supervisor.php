#!/usr/bin/env php
<?php namespace mef\Supervisor;

ini_set('memory_limit', '1M');

use Symfony\Component\Process\Process;

require_once __DIR__ . '/../vendor/autoload.php';

$wantsToQuit = false;

define('ONE_SECOND', 1000000);

pcntl_signal(SIGINT, function() use (&$wantsToQuit)
{
	$wantsToQuit = true;
});

$opts = getopt('', ['command:', 'workers:', 'success-delay:', 'error-delay:', 'quiet', 'silent']) +
	['command' => '', 'workers' => 1, 'success-delay' => 0, 'error-delay' => 5, 'quiet' => null, 'silent' => null];

$opts['workers'] = (int) $opts['workers'];
$opts['success-delay'] = (int) $opts['success-delay'];
$opts['error-delay'] = (int) $opts['error-delay'];

if ($opts['command'] === '' || $opts['workers'] < 1 || $opts['success-delay'] < 0 || $opts['error-delay'] < 0)
{
	echo 'Usage: ', PHP_EOL, "\t", $argv[0], ' --command="path to command" [--workers=1] [--success-delay=0] [--error-delay=5] [--silent] [--quiet]', PHP_EOL, PHP_EOL;
	echo "\t", '* command: the full path to the command', PHP_EOL;
	echo "\t", '* workers: the number of instances to run (default: 1)', PHP_EOL;
	echo "\t", '* success-delay: the number of seconds to delay before respawning a successfully terminated worker (default: 0)', PHP_EOL;
	echo "\t", '* error-delay: the number of seconds to delay before respawning a worker that terminated unexpectedly (default: 5)', PHP_EOL;
	echo "\t", '* silent: do not display any output from the workers', PHP_EOL;
	echo "\t", '* quiet: do not display any informational output from supervisor', PHP_EOL;
	echo PHP_EOL;
	exit;
}

$command = $opts['command'];
$count = $opts['workers'];
$delayOnSuccess = $opts['success-delay'];
$delayOnError = $opts['error-delay'];
$quiet = $opts['quiet'] !== null;
$silent = $opts['silent'] !== null;

$procs = [];

for ($i = 0; $i < $count; ++$i)
{
	$process = new Process($command);

	if ($silent === true)
	{
		$process->disableOutput();
	}

	$process->start();
	$procs[] = $process;
}

$restart = [];

$nextCollect = time() + 5;

while ($wantsToQuit === false)
{
	usleep(ONE_SECOND * .10);

	$now = time();

	foreach ($procs as $i => $process)
	{
		if ($silent === false)
		{
			fprintf(STDOUT, $process->getOutput());
			fprintf(STDERR, $process->getErrorOutput());
			$process->clearOutput();
			$process->clearErrorOutput();
		}

		if ($process->isRunning() === false)
		{
			if (array_key_exists($i, $restart) === false)
			{
				if ($process->isSuccessful() === false)
				{
					$restart[$i] = $now + $delayOnError;
				}
				else
				{
					$restart[$i] = $now + $delayOnSuccess;
				}
			}
			else if ($restart[$i] <= $now)
			{
				unset($restart[$i]);
				$process->start();
			}
		}
	}

	pcntl_signal_dispatch();
}

if ($quiet === false)
{
	echo 'Asking child processes to quit...';
}

foreach ($procs as $process)
{
	if ($process->isRunning() === true)
	{
		$process->signal(SIGINT);
	}
}

$stop = time() + 5;

do
{
	$anyRunning = false;

	foreach ($procs as $process)
	{
		if ($process->isRunning())
		{
			$anyRunning = true;
			usleep(ONE_SECOND * .01);
			break;
		}
	}
}
while (time() < $stop && $anyRunning === true);

if ($quiet === false)
{
	echo 'Done', PHP_EOL;
}
